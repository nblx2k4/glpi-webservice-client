<?php
    try
    {
        $soapArgs["method"] = "glpi.doLogout";
        $result = $soapClient->__call('genericExecute', array(new SoapParam($soapArgs,'param')));
        @session_destroy();
        $_SESSION = array();
        redirectTo('index.php');
    }catch (SoapFault $fault)
    {
        $errors = array($fault->faultcode);
        include 'ErrorViewer.php';
    }
?>
