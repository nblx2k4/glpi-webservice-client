<?php
    $soapArgs["method"] = "glpi.listAllMethods";
    try{
        $result = $soapClient->__call('genericExecute', array(new SoapParam($soapArgs,'param')));
        unset ($result["glpi.doLogin"],$result["glpi.doLogout"]);
        include 'templates/ShowAllMethods.php';
    }catch (SoapFault $fault)
    {
        $errors = array($fault->faultcode);
        include 'templates/ErrorViewer.php';
    }
?>
