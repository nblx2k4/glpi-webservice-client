<?php

$methodFunctions = NULL;

try
{
    $soapArgs["method"] = $_GET["method"];
    $methodFunctions = getFunctionArgs();
    
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $postKeys = array();
        foreach($_POST as $k => $v)
            $postKeys[] = $k;

        foreach($postKeys as $key)
        {
            if($_POST[$key] == NULL || $_POST[$key] == "")
                unset($_POST[$key]);
        }
        
        $soapArgs = array_merge($soapArgs,$_POST);
        $res = $soapClient->__call('genericExecute', array(new SoapParam($soapArgs,'param')));
        if(count($res) == 0)
        {
            $errors = array("La méthode n'a retourné aucun résultat.");
            include 'templates/ErrorViewer.php';
        }else
            var_dump($res);
    }else {
        include 'templates/MethodForm.php';
    }

}
catch (SoapFault $fault)
{
    $errors = array($fault->faultcode);
    include 'templates/ErrorViewer.php';
}
?>
