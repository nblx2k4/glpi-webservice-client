<?php

echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Authentification pour GLPI.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        #authForm
        {
            width: 400px;
            margin: 0 auto;
            margin-top: 30px;
            
        }
        #authForm form p
        {
            margin-top: 10px;
            overflow: auto;
        }
        #authForm label
        {
            display: block;
            width:150px;
            float:left;
        }
        #authForm input
        {
            display: block;
            width:120px;
            float:right;
        }
        #authForm form p input
        {
            display: block;
            width:250px;
            float:left;
        }
        .errors
        {
            color: red;
        }
    </style>
  </head>
  <body>
      <div id="authForm">';
            if(isset($errors) && (is_array($errors) && count($errors)>0))
            {
          echo '<ul class="errors">';

            foreach($errors as $error)
            {

              echo '<li>'.$error.'</li>';
            }
          echo '</ul>';
            }

          echo '<form method="post" action="'.$_SERVER["PHP_SELF"].'">
              <p>
                  <label for="username">Nom d\'utilisateur : </label><input type="text" name="Username" id="username" />
              </p>
              <p>
                  <label for="password">Mot de passe : </label><input type="password" name="Password" id="password" />
              </p>
              <input type="submit" value="S\'authentifier" />
          </form>
      </div>
  </body>
</html>';
